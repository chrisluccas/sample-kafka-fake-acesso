package br.com.mastertech.imersivo.controller


import br.com.mastertech.imersivo.kafka.model.Access
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import java.io.FileWriter
import java.nio.file.Files

@Component
class KafkaConsumer {

    @KafkaListener(topics = ["chrislucas"], groupId = "christoffer")
    fun listener(@Payload access: Access) {
        println(access)
        writeFile(access)
    }

    private fun writeFile(access: Access) {
        FileWriter("raw/log.csv", true).use {
            it.write("$access\n")
        }
    }

}