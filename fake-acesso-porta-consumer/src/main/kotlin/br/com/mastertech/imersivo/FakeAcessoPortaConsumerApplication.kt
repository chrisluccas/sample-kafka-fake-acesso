package br.com.mastertech.imersivo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FakeAcessoPortaConsumerApplication

fun main(args: Array<String>) {
	runApplication<FakeAcessoPortaConsumerApplication>(*args)
}
