package br.com.mastertech.imersivo.kafka.model

data class Access(var hasAccess: Boolean = false
                  , var time: Long = 0
                  , var clientId: Long = 0
                  , var doorId: Long = 0)