package br.com.mastertech.imersivo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FakeAcessoPortaPublisherApplication

fun main(args: Array<String>) {
	runApplication<FakeAcessoPortaPublisherApplication>(*args)
}
