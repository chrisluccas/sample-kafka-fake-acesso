package br.com.mastertech.imersivo.service

import br.com.mastertech.imersivo.kafka.model.Access
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service


@Service
class KafkaPublisher {

    companion object {
        private const val TOPIC = "chrislucas"
    }

    @Autowired
    private lateinit var publisher: KafkaTemplate<String, Access>

    fun send(access: Access) {
        publisher.send(TOPIC, "1", access)
    }
}