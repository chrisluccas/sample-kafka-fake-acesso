package br.com.mastertech.imersivo.controller

import br.com.mastertech.imersivo.service.KafkaPublisher
import br.com.mastertech.imersivo.kafka.model.Access
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import kotlin.random.Random

@RestController
class VerifyAccessController {

    @Autowired
    private lateinit var publisher : KafkaPublisher

    @GetMapping("acesso/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun verifyAccess(@PathVariable("cliente_id") clientId: Long,
                     @PathVariable("porta_id") doorId: Long) {
        publisher.send(randomResult(clientId, doorId))
        return;
    }

    private fun randomResult(clientId: Long, doorId: Long): Access {
        val time = System.currentTimeMillis()
        val random = Random(time)
                .nextLong(100, 1000) and 1L > 0

        return Access(random).apply {
            this.clientId = clientId
            this.doorId = doorId
            this.time = time
        }
    }
}